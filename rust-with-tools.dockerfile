# syntax=docker/dockerfile:1
FROM rust:latest AS base
RUN apt-get update -y && apt-get install -y --no-install-recommends \
    build-essential \
    libssl-dev \
    pkg-config \
    libclang-dev \
    docker.io \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*


# Install all tools individually, so that caching can be improved:
FROM base AS build-gitlab-report
RUN cargo install gitlab-report

FROM base AS build-cross
RUN cargo install cross

FROM base AS build-sccache
RUN cargo install sccache

FROM base AS build-cargo-audit
RUN cargo install cargo-audit


FROM base AS build-mdbook
RUN cargo install mdbook

FROM base AS final

COPY --from=build-gitlab-report /usr/local/cargo/bin/gitlab-report /usr/local/cargo/bin/gitlab-report
COPY --from=build-cross /usr/local/cargo/bin/cross /usr/local/cargo/bin/cross
COPY --from=build-sccache /usr/local/cargo/bin/sccache /usr/local/cargo/bin/sccache
COPY --from=build-cargo-audit /usr/local/cargo/bin/cargo-audit /usr/local/cargo/bin/cargo-audit
COPY --from=build-mdbook /usr/local/cargo/bin/mdbook /usr/local/cargo/bin/mdbook
