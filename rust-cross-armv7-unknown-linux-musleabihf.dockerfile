# syntax=docker/dockerfile:1
FROM rustembedded/cross:armv7-unknown-linux-musleabihf AS sccache-builder

RUN apt-get update && \
    apt-get install --assume-yes libssl-dev

RUN curl https://sh.rustup.rs -sSf | sh -s -- --profile minimal -y && \
    /root/.cargo/bin/cargo install sccache --root /
# /bin/sccache is now available

FROM rustembedded/cross:armv7-unknown-linux-musleabihf

# Install libclang for generating bindings with rust-bindgen
# The architecture is not relevant here since it's not used for compilation
RUN apt-get update && \
    apt-get install --assume-yes libclang-dev

COPY --from=sccache-builder /bin/sccache /sccache

# Set the target prefix
ENV TARGET_PREFIX="/usr/local/arm-linux-musleabihf"

# Make sure that cc-rs links libc/libstdc++ statically when cross-compiling
# See https://github.com/alexcrichton/cc-rs#external-configuration-via-environment-variables for more information
ENV RUSTFLAGS="-L$TARGET_PREFIX/lib" CXXSTDLIB="static=stdc++"

# Forcefully linking against libatomic, libc and libgcc is required for arm32, otherwise symbols are missing
ENV RUSTFLAGS="$RUSTFLAGS -Clink-arg=-static-libgcc -Clink-arg=-lgcc -lstatic=atomic -lstatic=c"

# Make sure that rust-bindgen uses the correct include path when cross-compiling
# See https://github.com/rust-lang/rust-bindgen#environment-variables for more information
ENV BINDGEN_EXTRA_CLANG_ARGS="-I$TARGET_PREFIX/include"
